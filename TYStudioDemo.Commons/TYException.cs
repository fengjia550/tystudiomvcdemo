﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TYStudioDemo.Commons
{
    [Serializable]
    public class TYException : Exception
    {
        //用来记录抛异常的方法，文件名和行号
        public static string CurrentFunction = "Function";
        public static string CurrentFilename = "Filename";
        public static string CurrentLineNumber = "Line Number";

        //定义够方法，调用Exception基类的构造方法
        public TYException() : base() { }
        public TYException(string message) : base(message) { }
        public TYException(string message, System.Exception inner) : base(message, inner) { }
        protected TYException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        //键值对的构造方法
        //把键值对存入Data,最终通过Enterprise library text foramtter存入数据库中
        public TYException(string message, Exception inner, IDictionary<string, string> props)
            : base(message, inner)
        {
            foreach (KeyValuePair<string, string> entry in props)
                if (!String.IsNullOrEmpty(entry.Key) && props[entry.Key] != null)
                    Data[entry.Key] = entry.Key;
        }

        //获得错误属性值
        public string GetErrorProperty(string key)
        {
            return (Data[key] == null) ? string.Empty : Data[key].ToString();
        }

        //添加错误属性值
        public void SetErrorProperty(string key, string value)
        {
            if (!String.IsNullOrEmpty(key) && value != null)
                Data[key] = value;
        }
    }
}
