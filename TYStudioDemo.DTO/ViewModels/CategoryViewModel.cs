﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TYStudioDemo.DTO
{
    public class CategoryViewModel
    {
        [Key]
        [Display(Name = "编号")]
        public int CategoryID { set; get; }

        [Required(ErrorMessage="请填写分类名称！")]
        [Display(Name="分类名称")]
        public string CategoryName { set; get; }

        [Display(Name = "分类描述")]
        [DataType(DataType.MultilineText)]
        public string Description { set; get; }

        [Display(Name = "分类图片")]
        public string Picture { set; get; }
    }
}
