﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TYStudioDemo.DTO
{
    public class ProductViewModel
    {
        [Key]
        [Display(Name="编号")]
        public int ProductID { set; get; }

        [Display(Name = "产品名称")]
        public string ProductName { set; get; }

        [Display(Name = "供应商")]
        public int SupplierID { set; get; }

        [Display(Name = "分类")]
        public int CategoryID { set; get; }

        [Display(Name = "单位数量")]
        public string QuantityPerUnit { set; get; }

        [Display(Name = "单价")]
        public decimal UnitPrice { set; get; }

        public int UnitsInStock { set; get; }

        public int UnitsOnOrder { set; get; }

        public int ReorderLevel { set; get; }

        public bool Discontinued { set; get; }
    }
}
