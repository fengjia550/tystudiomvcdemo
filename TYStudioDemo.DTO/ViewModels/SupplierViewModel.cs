﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TYStudioDemo.DTO
{
    public class SupplierViewModel
    {
        [Key]
        [Display(Name = "编号")]
        public int SupplierID { set; get; }

        [Required(ErrorMessage = "请填写公司名称")]
        [Display(Name = "公司名称")]
        public string CompanyName { set; get; }

        [Display(Name = "联系名称")]
        public string ContactName { set; get; }

        [Display(Name = "联系标题")]
        public string ContactTitle { set; get; }

        [Display(Name = "地址")]
        public string Address { set; get; }

        [Display(Name = "城市")]
        public string City { set; get; }

        [Display(Name = "地区")]
        public string Region { set; get; }

        [Display(Name = "邮编")]
        public string PostalCode { set; get; }

        [Display(Name = "国家")]
        public string Country { set; get; }

        [Display(Name = "电话")]
        public string Phone { set; get; }

        [Display(Name = "传真")]
        public string Fax { set; get; }

        [Display(Name = "公司网站")]
        public string HomePage { set; get; }
    }
}