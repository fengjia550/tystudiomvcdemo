﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYStudioDemo.Interfaces
{
    public interface IProductRepository<T> : ITYRepository<T> where T : class
    {
    }
}
