﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYStudioDemo.Interfaces
{
    public interface ISupplierRepository<T> : ITYRepository<T> where T : class
    {
        //声明一个通过companyName获得supplier的方法
        //注意这个方法是ITYRepository没有声明的，也就是我们扩展的业务方法。
        //其实这个方法完全可以放到Service，这里只是介绍一下ISupplierRepository接口的用法
        T GetByCompanyName(string companyName);
    }
}