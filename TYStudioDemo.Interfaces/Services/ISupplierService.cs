﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYStudioDemo.DTO;

namespace TYStudioDemo.Interfaces
{
    /// <summary>
    /// 声明Supplier供应商独有的业务逻辑方法
    /// 
    /// 本教程声明了CRUD和Search方法
    /// </summary>
    public interface ISupplierService : ITYService
    {
        IEnumerable<SupplierViewModel> GetAll();
        SupplierViewModel GetByID(int ID);
        void Create(SupplierViewModel dto);
        void Delete(int ID);
        void Update(SupplierViewModel dto);
        IEnumerable<SupplierViewModel> SearchByCriteria(string companyName);
    }
}
