﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYStudioDemo.Interfaces
{
    public interface ITYExceptionService
    {
        string HandleException(Exception exception, Dictionary<string, object> parms, string message);
    }
}
