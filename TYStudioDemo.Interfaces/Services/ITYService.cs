﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYStudioDemo.Interfaces
{
    /// <summary>
    /// Service的顶级接口，所有Service类都要实现此接口
    /// 声明Service公共的方法，根据需求可以加入自己想要的方法
    /// 例如：管理系统可以加入CRUD的方法
    /// 
    /// 本教程只声明了一个将Entity转化为DTO的方法
    /// </summary>
    public interface ITYService
    {
        /// <summary>
        /// 将Entity转化成ViewModel对象
        /// 这样做是为了使View，也就是UI层和Model database层分开，降低耦合度
        /// 因为所有View都将使用ViewModel，而不是Entity
        /// 
        /// 这里指定object为所有的参数和返回类型
        /// 因为虽有的对象都是从object继承下来的
        /// 将来你传进来什么他就是什么~是不是很酷，那句话叫什么来着：父类引用指向子类对象
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        object ConvertToViewModel(object entity);
    }
}
