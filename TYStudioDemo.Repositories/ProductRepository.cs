﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYStudioDemo.Models;
using TYStudioDemo.Interfaces;

namespace TYStudioDemo.Repositories
{
    public class ProductRepository: TYRepository<Product>, IProductRepository<Product>
    {

    }
}
