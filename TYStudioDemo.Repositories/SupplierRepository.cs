﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYStudioDemo.Interfaces;
using TYStudioDemo.Models;

namespace TYStudioDemo.Repositories
{
    public class SupplierRepository : TYRepository<Supplier>, ISupplierRepository<Supplier>
    {
        //实现ISupplierRepository中的方法

        #region ISupplierRepository<Supplier> Members

        public Supplier GetByCompanyName(string companyName)
        {
            return TYEntities.Current.Suppliers.SingleOrDefault(e=>e.CompanyName == companyName);
        }

        #endregion
    }
}