﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYStudioDemo.Interfaces;
using TYStudioDemo.DTO;
using TYStudioDemo.Models;
using TYStudioDemo.Commons;

namespace TYStudioDemo.Services
{
    public class SupplierService : ISupplierService
    {
        ISupplierRepository<Supplier> _supplierRepository;
        IProductRepository<Product> _productRepository;

        public SupplierService(ISupplierRepository<Supplier> supplierRepotitory,
                                    IProductRepository<Product> productRepository)
        {
            _supplierRepository = supplierRepotitory;
            _productRepository = productRepository;
        }

        public IEnumerable<SupplierViewModel> GetAll()
        {
            List<SupplierViewModel> list = new List<SupplierViewModel>();
            foreach(Supplier entity in _supplierRepository.GetAll())
            {
                list.Add(ConvertToViewModel(entity) as SupplierViewModel);
            }
            return list.AsEnumerable<SupplierViewModel>();
        }

        public SupplierViewModel GetByID(int ID)
        {
            return ConvertToViewModel(_supplierRepository.Single(e => e.SupplierID == ID)) as SupplierViewModel;
        }

        public void Create(SupplierViewModel vModel)
        {
            try
            {
                Supplier supplier = new Supplier();
                supplier.SupplierID = vModel.SupplierID;
                supplier.Address = vModel.Address;
                supplier.City = vModel.City;
                supplier.CompanyName = vModel.CompanyName;
                supplier.ContactName = vModel.ContactName;
                supplier.ContactTitle = vModel.ContactTitle;
                supplier.Country = vModel.Country;
                supplier.Fax = vModel.Fax;
                supplier.HomePage = vModel.HomePage;
                supplier.Phone = vModel.Phone;
                supplier.PostalCode = supplier.PostalCode;
                supplier.Region = vModel.Region;
                _supplierRepository.Create(supplier);

                //为了显示事务的处理，这里添加一个临时的Product
                //由于Product数据并没有通过vModel传递过来，此处只是测试数据。
                Product product = new Product();

                //给product属性赋值
                //其他的属性略去，只写一个必须的名称
                product.ProductName = "testProductName";

                //虽然还没有调用datacontext的保存方法，没有在数据库中生成supplierid，
                //但是EntityFramework内部会指定一个新的supplierid，所以这里supplierid是有值的
                product.SupplierID = supplier.SupplierID;

                _productRepository.Create(product);

                //TYEntities.Current保存修改数据
                //当处理多个表时，只需要调用一次SaveChanges()，所以都在一个Transaction里面。
                TYEntities.Current.SaveChanges();
            }
            catch (Exception ex)
            {
                //虽然此处谢了try catch语句，但是我们并不处理这里的异常，继续throw到Controller层
                //所有异常都在Controller层catch，返回错误信息。
                //此处的try catch不是必须的，当让如果你想完善异常信息的处理，这里可以自定义你的异常信息。
                Exception exception = new Exception("添加Supplier出错" + ex.Message);

                //抛到Controller层
                throw exception;
            }
        }

        public void Delete(int ID)
        {
            Supplier entity = _supplierRepository.Single(e => e.SupplierID == ID);
            _supplierRepository.Delete(entity);
            TYEntities.Current.SaveChanges();
        }

        public void Update(SupplierViewModel vModel)
        {
            Supplier entity = _supplierRepository.Single(e => e.SupplierID == vModel.SupplierID);
            entity.SupplierID = vModel.SupplierID;
            entity.Address = vModel.Address;
            entity.City = vModel.City;
            entity.CompanyName = vModel.CompanyName;
            entity.ContactName = vModel.ContactName;
            entity.ContactTitle = vModel.ContactTitle;
            entity.Country = vModel.Country;
            entity.Fax = vModel.Fax;
            entity.HomePage = vModel.HomePage;
            entity.Phone = vModel.Phone;
            entity.PostalCode = entity.PostalCode;
            entity.Region = vModel.Region;

            
            TYEntities.Current.SaveChanges();
        }

        public object ConvertToViewModel(object entity)
        {
            Supplier supplier = (Supplier)entity;
            SupplierViewModel vModel = new SupplierViewModel();
            vModel.SupplierID = supplier.SupplierID;
            vModel.Address = supplier.Address;
            vModel.City = supplier.City;
            vModel.CompanyName = supplier.CompanyName;
            vModel.ContactName = supplier.ContactName;
            vModel.ContactTitle = supplier.ContactTitle;
            vModel.Country = supplier.Country;
            vModel.Fax = supplier.Fax;
            vModel.HomePage = supplier.HomePage;
            vModel.Phone = supplier.Phone;
            vModel.PostalCode = supplier.PostalCode;
            vModel.Region = supplier.Region;
            return vModel;
        }

        /// <summary>
        /// 搜索方法，参数根据需求可以自由添加
        /// </summary>
        /// <param name="companyName"></param>
        /// <returns></returns>
        public IEnumerable<SupplierViewModel> SearchByCriteria(string companyName)
        {
            //此处实现你的搜索
            throw new NotImplementedException();
        }

    }
}
