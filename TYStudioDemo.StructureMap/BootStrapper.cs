﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructureMap;

namespace TYStudioDemo.StructureMap
{
    public class BootStrapper
    {
        public static void ConfigureStructureMap()
        {
            ObjectFactory.Configure(x =>
            {
                x.AddRegistry(new TYStudioDemoStructureMapRegistry());

                x.Scan(scanner =>
                {
                    scanner.Assembly("TYStudioDemo.Services");
                    scanner.Assembly("TYStudioDemo.Repositories");
                });
            });
        }
    }
}
