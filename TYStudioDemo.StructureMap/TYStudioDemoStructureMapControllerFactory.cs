﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using StructureMap;

namespace TYStudioDemo.StructureMap
{
    public class TYStudioDemoStructureMapControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, System.Type controllerType)
        {
            if (controllerType != null)
            {
                Controller c = ObjectFactory.GetInstance(controllerType) as Controller;

                //当返回一个错误页面，View一级异常会被触发
                c.ActionInvoker = new ErrorHandlingActionInvoker(new HandleErrorAttribute());
                return c;
            }
            else
                return null;
        }
    }
}
