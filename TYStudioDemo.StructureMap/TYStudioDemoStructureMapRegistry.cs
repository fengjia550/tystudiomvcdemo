﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using StructureMap.Configuration.DSL;
using TYStudioDemo.Models;
using TYStudioDemo.DTO;
using TYStudioDemo.Interfaces;
using TYStudioDemo.Services;
using TYStudioDemo.Repositories;

namespace TYStudioDemo.StructureMap
{
    public class TYStudioDemoStructureMapRegistry : Registry
    {
        //注册接口实际使用的实现类
        public TYStudioDemoStructureMapRegistry()
        {
            SelectConstructor<TYEntities>(() => new TYEntities());

            //Exception Handle
            For<ITYExceptionService>().Use<TYExceptionService>();

            //Services
            For(typeof(ISupplierService)).Use(typeof(SupplierService));

            //Repositories
            For(typeof(ISupplierRepository<>)).Use(typeof(SupplierRepository));
            For(typeof(IProductRepository<>)).Use(typeof(ProductRepository));
        }
    }
}
