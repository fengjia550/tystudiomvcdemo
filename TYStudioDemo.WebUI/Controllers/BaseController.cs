﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TYStudioDemo.Interfaces;

namespace TYStudioDemo.WebUI.Controllers
{
    public abstract class BaseController : Controller
    {
         protected ITYExceptionService _tyExceptionService;

         public BaseController() { }

         public BaseController(ITYExceptionService tyExceptionService)
         {
             _tyExceptionService = tyExceptionService;
         }
    }
}
