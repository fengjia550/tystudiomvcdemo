﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TYStudioDemo.DTO.SupplierViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Create</h2>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>SupplierDTO</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CompanyName) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CompanyName) %>
            <%: Html.ValidationMessageFor(model => model.CompanyName) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.ContactName) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.ContactName) %>
            <%: Html.ValidationMessageFor(model => model.ContactName) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.ContactTitle) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.ContactTitle) %>
            <%: Html.ValidationMessageFor(model => model.ContactTitle) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Address) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Address) %>
            <%: Html.ValidationMessageFor(model => model.Address) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.City) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.City) %>
            <%: Html.ValidationMessageFor(model => model.City) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Region) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Region) %>
            <%: Html.ValidationMessageFor(model => model.Region) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PostalCode) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.PostalCode) %>
            <%: Html.ValidationMessageFor(model => model.PostalCode) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Country) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Country) %>
            <%: Html.ValidationMessageFor(model => model.Country) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Phone) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Phone) %>
            <%: Html.ValidationMessageFor(model => model.Phone) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Fax) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Fax) %>
            <%: Html.ValidationMessageFor(model => model.Fax) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.HomePage) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.HomePage) %>
            <%: Html.ValidationMessageFor(model => model.HomePage) %>
        </div>

        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%: Scripts.Render("~/bundles/jqueryval") %>
</asp:Content>
