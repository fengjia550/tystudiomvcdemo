﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TYStudioDemo.DTO.SupplierViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Delete
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<fieldset>
    <legend>SupplierDTO</legend>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.CompanyName) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.CompanyName) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.ContactName) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.ContactName) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.ContactTitle) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.ContactTitle) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Address) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Address) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.City) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.City) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Region) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Region) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.PostalCode) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.PostalCode) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Country) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Country) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Phone) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Phone) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Fax) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Fax) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.HomePage) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.HomePage) %>
    </div>
</fieldset>
<% using (Html.BeginForm()) { %>
    <p>
        <input type="submit" value="Delete" /> |
        <%: Html.ActionLink("Back to List", "Index") %>
    </p>
<% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>
