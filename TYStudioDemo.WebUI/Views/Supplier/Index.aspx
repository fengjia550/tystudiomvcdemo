﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<TYStudioDemo.DTO.SupplierViewModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Index</h2>

<p>
    <%: Html.ActionLink("Create New", "Create") %>
</p>
<table>
    <tr>
        <th>
            <%: Html.DisplayNameFor(model => model.CompanyName) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.ContactName) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.ContactTitle) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Address) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.City) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Region) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.PostalCode) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Country) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Phone) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Fax) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.HomePage) %>
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.CompanyName) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.ContactName) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.ContactTitle) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Address) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.City) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Region) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.PostalCode) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Country) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Phone) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Fax) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.HomePage) %>
        </td>
        <td>
            <%: Html.ActionLink("Edit", "Edit", new { id=item.SupplierID }) %> |
            <%: Html.ActionLink("Details", "Details", new { id=item.SupplierID }) %> |
            <%: Html.ActionLink("Delete", "Delete", new { id=item.SupplierID }) %>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>
